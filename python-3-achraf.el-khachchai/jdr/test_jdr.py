#!/usr/bin/env python3

import unittest
import itertools
from jdr import *

class TestJdr(unittest.TestCase):

    def test_add_repr(self):
        p1 = -3 + D6() + 2 + d8 + -4
        self.assertEqual(repr(p1), '-3 + D6 + 2 + D8 + -4')

    def test_add_throw_show(self):
        AbstractResult.seed(123)
        p1 = 2 + D6() + d8 + -7
        self.assertEqual(p1.throw(), 1)
        self.assertEqual(p1.show(), '2, 1, 5, -7')

    def test_sub_repr(self):
        p1 = D6() - 2 - d8 - -1
        self.assertEqual(repr(p1), 'D6 - 2 - D8 - -1')

    def test_sub_throw_show(self):
        AbstractResult.seed(123)
        p1 = 2 - D6() - d8 - -1
        self.assertEqual(p1.throw(), -3)
        self.assertEqual(p1.show(), '2, 1, 5, -1')

    def test_mult_repr(self):
        p1 = d6 * -4 + 12 + 4 * D8() - 3
        self.assertEqual(repr(p1), '-4D6 + 12 + 4D8 - 3')

    def test_mult_throw_show(self):
        AbstractResult.seed(123)
        p1 = d6 * -4 + 12 + 4 * D8() - 5
        self.assertEqual(p1.throw(), 13)
        self.assertEqual(p1.show(), '1, 3, 1, 4, 12, 5, 2, 1, 7, 5')

    def test_type_roll_name(self):
        self.assertEqual(type(d4.roll()).__name__, 'generator')

    def test_D4_D6_roll(self):
        prod = itertools.product(range(1, 7), range(1, 5))
        for res, roll in zip(prod, (D6() + D4()).roll()):
            self.assertEqual(res, roll)

    def test_D4_D6_D12_roll(self):
        prod = itertools.product(range(1, 7), range(1, 5), range(1, 13))
        for res, roll in zip(prod, (D6() - D4() + d12).roll()):
            self.assertEqual(res, roll)

    def test_3D8_1_2D6_12_roll(self):
        iterables = [range(1, 9), range(1, 9), range(1, 9), range(1, 2),
                     range(1, 7), range (1, 7), range(-12, -11)]
        prod = itertools.product(*iterables)
        for res, roll in zip(prod, (3 * d8 + 1 + D6() * 2 + -12).roll()):
            self.assertEqual(res, roll)

    def test_success(self):
        proba = (D6() * 6).success(lambda x: sum(x) >= 30)
        self.assertEqual(proba, 1.9675925925925926)
        proba = (D6() * 6).success(lambda x: sum(x) >= 1)
        self.assertEqual(proba, 100)
        proba = (d8 + d6 + d4).success(lambda x: x[0] + x[1] - x[2] > 0)
        self.assertAlmostEqual(proba, 94.7916, 3)

    def test_darkness(self):
        proba = (D10() * 5).success(Pool.darkness)
        self.assertEqual(proba, 33.696)
        proba = (10 * d4).success(Pool.darkness)
        self.assertEqual(proba, 0)
        proba = (4 * d10).success(Pool.darkness)
        self.assertAlmostEqual(proba, 12.959, 2)

if __name__ == '__main__':
    unittest.main()
