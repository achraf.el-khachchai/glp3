import copy
import random
import itertools

### 

class AddExpr:
    def __init__(self, lhs, rhs):
        self.lhs = lhs
        self.rhs = rhs
        self.result = 0

    def __repr__(self) -> str:
        return repr(self.lhs) + ' + ' + repr(self.rhs)

    def throw(self) -> int:
        if self.result == 0:
            a = self.lhs.throw()
            b = self.rhs.throw()
            self.result = a + b
        return self.result

    def show(self) -> str:
        return '{}, {}'.format(self.lhs.show(), self.rhs.show())

    def _range(self) -> [range]:
        return [*self.lhs._range(), *self.rhs._range()]

class SubExpr:

    def __init__(self, lhs, rhs):
        self.lhs = lhs
        self.rhs = rhs
        self.result = 0

    def __repr__(self) -> str:
        return repr(self.lhs) + ' - ' + repr(self.rhs)

    def throw(self) -> int:
        if self.result == 0:
            a = self.lhs.throw()
            b = self.rhs.throw()
            self.result = a - b
        return self.result

    def show(self) -> str:
        return '{}, {}'.format(self.lhs.show(), self.rhs.show())

    def _range(self) -> [range]:
        return [*self.lhs._range(), *self.rhs._range()]

class MulExpr:
    def __init__(self, dice, mul):
        self.sign = 1 if mul >= 0 else -1
        self.dices = [copy.deepcopy(dice) for i in range(abs(mul))]
        self.result = 0

    def __repr__(self) -> str:
        sign = '' if self.sign == 1 else '-'
        return f'{sign}{len(self.dices)}{self.dices[0]}'

    def throw(self) -> int:
        if self.result == 0:
            dicethrow = lambda x: x.throw()
            self.result = sum(map(dicethrow, self.dices))
        return self.sign * self.result

    def show(self) -> str:
        return ', '.join(map(lambda x: x.show(), self.dices))

    def _range(self) -> [range]:
        return [range(1, dice.max + 1) for dice in self.dices]

class Pool:

    darkness = lambda x: len(list(filter(lambda y: y >= 5, x))) >= 4

    def __init__(self, op):
        self.op = op

    def throw(self):
        return self.op.throw()

    def show(self):
        return self.op.show()

    def roll(self):
        iterables = [*self.op._range()]
        for prod in itertools.product(*iterables):
            yield prod

    def success(self, check):
        total, valid = 0, 0
        for roll in self.roll():
            total = total + 1
            if check(roll):
                valid = valid + 1
        return 100 * valid / total

    def _range(self):
        return self.op._range()

    def __add__(self, rhs):
        if type(rhs) is int:
            rhs = FrozenDice(rhs)
        return Pool(AddExpr(self, rhs))

    def __radd__(self, lhs):
        if type(lhs) is int:
            lhs = FrozenDice(lhs)
        return Pool(AddExpr(lhs, self))

    def __sub__(self, rhs):
        if type(rhs) is int:
            rhs = FrozenDice(rhs)
        return Pool(SubExpr(self, rhs))

    def __rsub__(self, lhs):
        if type(lhs) is int:
            lhs = FrozenDice(lhs)
        return Pool(SubExpr(lhs, self))

    def __mul__(self, rhs):
        return Pool(MulExpr(self, rhs))

    def __rmul__(self, lhs):
        return Pool(MulExpr(self, lhs))

    def __repr__(self) -> str:
        return repr(self.op)

class AbstractResult(Pool):
    min = 1
    max = 1
    def __init__(self):
        self.result = 0

    def __repr__(self):
        return type(self).__name__

    def throw(self):
        if self.result == 0:
            self.result = random.randint(type(self).min, type(self).max)
        return self.result

    def show(self):
        return str(self.result)

    def seed(s):
        random.seed(s)

    def _range(self):
        return [range(1, self.max + 1)]

class FrozenDice(AbstractResult):
    def __init__(self, v):
        self.result = v
        self.min = v
        self.max = v

    def __repr__(self):
        return str(self.result)

    def throw(self):
        return self.result

    def _range(self):
        return [range(self.max, self.max + 1)]

####

class D4(AbstractResult):
        max=4

d4 = D4()

class D6(AbstractResult):
        max=6

d6 = D6()

class D8(AbstractResult):
        max=8

d8 = D8()

class D10(AbstractResult):
        max=10

d10 = D10()

class D12(AbstractResult):
        max=12

d12 = D12()

class D20(AbstractResult):
        max=20

d20 = D20()
