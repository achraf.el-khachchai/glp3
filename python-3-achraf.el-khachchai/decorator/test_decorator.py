#!/usr/bin/env python3

import unittest
from decorator import *
import pyrser.meta as pm

class TestCheckTypes(unittest.TestCase):

    @classmethod
    def dec_and_pyr(func, *args, **kw):

        def res_or_err(func, *args, **kw):
            try:
                return func(*args, **kw)
            except Exception as err:
                return err.args[0]

        dec = checktypes(func)
        pyr = pm.checktypes(func)
        return res_or_err(dec, *args, **kw), res_or_err(pyr, *args, **kw)

    class Pouet(object):

        def __init__(self, *args):
            self.__dict__.update({
                f'_{str(i)}': v for i, v in zip(range(len(args)), args)
            })

        def __repr__(self):
            return repr(vars(self))

    def test_vanilla(self):
        def f(a, b, c, *args, **kw):
            return f'cool {locals()!r}'
        dec, pyr = self.dec_and_pyr(f, 'a', 1, 2, 3, 'b', d='D', e=5)
        self.assertEqual(dec, pyr)

    def test_vanilla_valid_return(self):
        def f(a, b, c, *args, **kw) -> str:
            return f'cool {locals()!r}'
        dec, pyr = self.dec_and_pyr(f, 'a', 1, 2, 3, 'b', d='D', e=5)
        self.assertEqual(dec, pyr)

    def test_vanilla_fail_return(self):
        def f(a, b, c, *args, **kw) -> int:
            return f'cool {locals()!r}'
        dec, pyr = self.dec_and_pyr(f, 'a', 1, 2, 3, 'b', d='D', e=5)
        self.assertEqual(dec, pyr)

    def test_valid_positional(self):
        def f(F: str, E: int, G, *args, **kw):
            return f'cool {locals()!r}'
        dec, pyr = self.dec_and_pyr(f, 'a', 1, 2, 3, 'b', d='D', e=5)
        self.assertEqual(dec, pyr)

    def test_valid_var_positional(self):
        def f(F: str, E: int, G, *args: int, **kw):
            return f'cool {locals()!r}'
        dec, pyr = self.dec_and_pyr(f, 'a', 1, 2, 3, 4, d='D', e=5)
        self.assertEqual(dec, pyr)

    def test_valid_var_keyword(self):
        def f(F: str, E: int, G, *args, **kw: str):
            return f'cool {locals()!r}'
        dec, pyr = self.dec_and_pyr(f, 'a', 1, 2, 3, 'b', d='D', e='E')
        self.assertEqual(dec, pyr)

    def test_valid_positional_valid_return(self):
        def f(F: str, E: int, G, *args, **kw) -> str:
            return f'cool {locals()!r}'
        dec, pyr = self.dec_and_pyr(f, 'a', 1, 2, 3, 'b', d='D', e=5)
        self.assertEqual(dec, pyr)

    def test_valid_positional_fail_return(self):
        def f(F: str, E: int, G, *args, **kw) -> str:
            return 0
        dec, pyr = self.dec_and_pyr(f, 'a', 1, 2, 3, 'b', d='D', e=5)
        self.assertEqual(dec, pyr)

    def test_first_positional_fail(self):
        def f(F: str, E: int, G, *args, **kw):
            return f'cool {locals()!r}'
        dec, pyr = self.dec_and_pyr(f, 'a', 1, 2, 3, 'b', d='D', e=5)
        self.assertEqual(dec, pyr)

    def test_second_positional_fail(self):
        def f(F: str, E: int, G, *args, **kw):
            return f'cool {locals()!r}'
        dec, pyr = self.dec_and_pyr(f, 'a', 1, 2, 3, 'b', d='D', e=5)
        self.assertEqual(dec, pyr)

    def test_fail_var_positional(self):
        def f(F: str, E: int, G, *args: str, **kw):
            return f'cool {locals()!r}'
        dec, pyr = self.dec_and_pyr(f, 'a', 1, 2, 3, 'b', d='D', e=5)
        self.assertEqual(dec, pyr)

    def test_fail_var_keyword(self):
        def f(F: str, E: int, G, *args, **kw: int):
            return f'cool {locals()!r}'
        dec, pyr = self.dec_and_pyr(f, 'a', 1, 2, 3, 'b', d='D', e=5)
        self.assertEqual(dec, pyr)

    def test_first_positional_fail_return_valid(self):
        def f(F: str, E: int, G, *args, **kw) -> str:
            return f'cool {locals()!r}'
        dec, pyr = self.dec_and_pyr(f, 'a', 1, 2, 3, 'b', d='D', e=5)
        self.assertEqual(dec, pyr)

    def test_second_positional_fail_return_fail(self):
        def f(F: str, E: int, G, *args, **kw) -> float:
            return f'cool {locals()!r}'
        dec, pyr = self.dec_and_pyr(f, 'a', 1, 2, 3, 'b', d='D', e=5)
        self.assertEqual(dec, pyr)

    def test_class_valid(self):
        def f(a: int, b: float, c: self.Pouet) -> str:
            return f'cool {locals()!r}'
        p = self.Pouet('cool', 42)
        dec, pyr = self.dec_and_pyr(f, 1, 1.2, p, 3, 'b', d='D', e=5)
        self.assertEqual(dec, pyr)

    def test_class_valid(self):
        def f(a: int, b: float, c: self.Pouet) -> str:
            return f'cool {locals()!r}'
        dec, pyr = self.dec_and_pyr(f, 1, 1.2, 2, 3, 'b', d='D', e=5)
        self.assertEqual(dec, pyr)


if __name__ == '__main__':
    unittest.main()
