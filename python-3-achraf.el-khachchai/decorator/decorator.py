import inspect
from functools import wraps

def checktypes(f):

    validtype = lambda x: inspect.isclass(x) and x is not sig.empty

    sig = inspect.signature(f)
    arg_dict = {
        arg_name: arg
        for arg_name, arg in sig.parameters.items()
        if validtype(arg.annotation)
    }

    def checktype(arg, arg_name, arg_type):
        if type(arg) is not arg_type:
            raise TypeError(f"{f.__qualname__}: wrong type of "
                f"'{arg_name}' argument, '{arg_type.__name__}' expected"
                f", got '{type(arg).__name__}'")


    @wraps(f)
    def wrapper(*args, **kwargs):
        bound = sig.bind(*args, kwargs)
        for arg_name, arg in arg_dict.items():
            arg_type = arg.annotation
            if arg.kind == inspect.Parameter.VAR_POSITIONAL:
                for pos_arg in bound.arguments[arg_name]:
                    checktype(pos_arg, arg_name, arg_type)
            elif arg.kind == inspect.Parameter.VAR_KEYWORD:
                for kw_name, kw_arg in bound.arguments[arg_name].items():
                    checktype(kw_arg, f'{arg_name}:{kw_name}', arg_type)
            else:
                checktype(bound.arguments[arg_name], arg_name, arg_type)

        res = f(*args, kwargs)
        res_t = sig.return_annotation
        if validtype(res_t) and type(res) is not res_t:
            raise TypeError(f"{f.__qualname__}: wrong return type, "
                f"{res_t.__name__} expected, got {type(res).__name__}")
        return res

    return wrapper
