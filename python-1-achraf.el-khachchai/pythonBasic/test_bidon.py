#!/usr/bin/env python3

import unittest
import bidon

class TestBidon(unittest.TestCase):

    def test_cool(self):
        a = bidon.Bidon('cool')
        assert vars(a) == { 'txt': 'cool', 'num': 42 }

    def test_satan(self):
        a = bidon.Bidon("la bête", 666)
        assert a.num == 666

    def test_seum(self):
        a = bidon.Bidon('keywords var', 1024, le=4, nain=5, a=6, chausette=7)
        seum = a.le + a.nain + a.a + a.chausette
        assert seum == 22

class TestVar2listsort(unittest.TestCase):

    def test_int(self):
        assert bidon.var2listsort(3, 1, 7, 2) == [1, 2, 3, 7]

    def test_float(self):
        l = bidon.var2listsort(40.3, 40.0, 39.8, 38.7, 39.9, 40.1, 38.80, 38.69)
        assert l == [38.69, 38.7, 38.8, 39.8, 39.9, 40.0, 40.1, 40.3]

if __name__ == '__main__':
    unittest.main()
