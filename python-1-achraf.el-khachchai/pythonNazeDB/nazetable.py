import os
import csv
from nazedb import *

class NazeTable:

    def __init__(self, tname: str, db: NazeDB):
        self.tname = tname
        self.db = db
        self.load()

    def set_header(self, ls: [str]):
        self.header = ls

    def get_header(self) -> [str]:
        return self.header

    @property
    def filepath(self) -> str:
        return '{}/{}.csv'.format(self.db.dbinfos.dbpath, self.tname)

    @property
    def get_rows(self) -> [[str]]:
        return self.rows

    def add_row(self, **kw):
        if sorted(self.header) == sorted(list(kw.keys())):
            self.rows.append([ str(kw[k]) for k in self.header ])

    def save(self):
        with open(self.filepath, 'w') as csvfile:
            csvfile.write('{}\n'.format(','.join(str(h)
                for h in self.header)))
            csvfile.writelines(['{}\n'.format(','.join(str(r) for r in row))
                for row in self.rows])
        try:
            self.db._tablenames().index(self.tname)
        except ValueError:
            with open(self.db._indexpath(), 'a') as f:
                f.write('{}\n'.format(self.tname))

    def load(self):
        try:
            with open(self.filepath) as csvfile:
                rows = list(csv.reader(csvfile, delimiter=','))
                self.header = rows[0]
                self.rows = rows[1:]
        except FileNotFoundError:
            self.header = []
            self.rows = []
            open(self.filepath, 'a').close()

    def clean(self):
        os.remove(self.filepath)
        tablenames = self.db._tablenames()
        try:
            tablenames.remove(self.tname)
        except ValueError:
            pass
        with open(self.db._indexpath(), 'w') as f:
            f.writelines(['{}\n'.format(tname) for tname in tablenames])

    @staticmethod
    def cleanall(dbinfos: DBInfos):
        indexfile = '{}/{}'.format(dbinfos.dbpath, dbinfos.dbfile)
        with open(indexfile, 'r') as findex:
            tablenames = findex.read().split('\n')[:-1]
            for tname in tablenames:
                try:
                    os.remove('{}/{}.csv'.format(dbinfos.dbpath, tname))
                except:
                    pass
        os.remove(indexfile)
        open(indexfile, 'a').close()
