#!/usr/bin/env python3

import os
import re
import unittest
import pexpect
import nazedb
import nazetable

class TestNazeDB(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        dirs = ['pouet/nia/default', 'pouet/nu', 'testdb']
        dbs = ['pouet', 'pouet/nia/default', 'testdb']
        open_close_file = lambda f: open('{}/.index.db'.format(f), 'a').close()
        list(map(os.makedirs, dirs))
        list(map(open_close_file, dbs))

    @classmethod
    def tearDownClass(cls):
        dbs = ['pouet', 'pouet/nia/default', 'testdb', 'init']
        dirs = ['init', 'testdb', 'pouet/nia/default', 'pouet/nu']
        remove_index_file = lambda f: os.remove('{}/.index.db'.format(f))
        list(map(remove_index_file, dbs))
        list(map(os.removedirs, dirs))

    def test_tuple(self):
        infos = nazedb.DBInfos._make(['test', 'a/b/c', '.index.db'])
        s = "infos(dbname='test', dbpath='a/b/c', dbfile='.index.db')"
        assert str(infos) == s

    def test_paths(self):
        db = nazedb.NazeDB('default')
        assert db.dbinfos.dbpath == './pouet/nia/default'

        db.open('testdb')
        assert db.dbinfos.dbpath == './testdb'

    def test_create(self):
        db = nazedb.NazeDB('init')
        assert db.dbinfos.dbpath == './init'

        db.open('open')
        assert db.dbinfos is None

    def test_repr(self):
        dbs = [
                ['init', './init', '.index.db'],
                ['testdb', './testdb', '.index.db'],
                ['pouet', './pouet', '.index.db'],
                ['default', './pouet/nia/default', '.index.db']
        ]
        l = list(map(nazedb.DBInfos._make, dbs))
        assert nazedb.NazeDB.listdb() == l

class TestNazeTable(unittest.TestCase):

    @classmethod
    def tearDownClass(cls):
        files = ['.index.db', 'tab1.csv']
        for f in files:
            os.remove('testdb/{}'.format(f))
        os.removedirs('testdb')

    def setUp(self):
        self.ndb = nazedb.NazeDB('testdb')

    def test_1_create(self):
        nazetable.NazeTable.cleanall(self.ndb.dbinfos)
        tab1 = nazetable.NazeTable('tab1', self.ndb)
        assert os.path.exists(tab1.filepath)
        header = ['col1', 'col2', 'col3']
        tab1.set_header(header)
        assert tab1.get_header() == header
        tab1.add_row(col1=4, col2=5, col3=6)
        tab1.add_row(col3=9, col1=7, col2=8)
        rows = [['4', '5', '6'],
                ['7', '8', '9']]
        assert tab1.get_rows == rows
        tab1.save()
        with open('testdb/tab1.csv') as f:
            s = 'col1,col2,col3\n4,5,6\n7,8,9\n'
            assert f.read() == s

    def test_2_add(self):
        tab1 = nazetable.NazeTable('tab1', self.ndb)
        tab1.load()
        header = ['col1', 'col2', 'col3']
        tab1.set_header(header)
        assert tab1.get_header() == header
        tab1.add_row(**{'col1': 1, 'col2': 2, 'col3': 3})
        rows = [['4', '5', '6'],
                ['7', '8', '9'],
                ['1', '2', '3']]
        assert tab1.get_rows == rows
        tab1.save()
        with open('testdb/tab1.csv') as f:
            s = 'col1,col2,col3\n4,5,6\n7,8,9\n1,2,3\n'
            assert f.read() == s

class TestNazeCLI(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        dirs = ['pouet/nia/default', 'pouet/nu', 'testdb']
        dbs = ['pouet', 'pouet/nia/default', 'testdb']
        open_close_file = lambda f: open('{}/.index.db'.format(f), 'a').close()
        list(map(os.makedirs, dirs))
        list(map(open_close_file, dbs))
        with open('testdb/.index.db', 'w') as f:
            f.write('tab1\n')
        with open('testdb/tab1.csv', 'a') as f:
            f.write('col3,col1,col2\n3,1,2\n6,4,5\n9,7,8\n')

    @classmethod
    def tearDownClass(cls):
        dirs = ['pouet/nia/default', 'pouet/nu', 'testdb']
        dbs = ['pouet', 'pouet/nia/default', 'testdb']
        remove_index_file = lambda f: os.remove('{}/.index.db'.format(f))
        list(map(remove_index_file, dbs))
        list(map(os.removedirs, dirs))

    def test_cli(self):
        child = pexpect.spawn('./nazecli', timeout=5)
        cmds = [
            'listdb',
            'showdb',
            'opendb testdb',
            'showdb',
            'listtable',
            'desc tab1',
            'showtable tab1',
            'insert tab1(8,9,10)',
            'showtable tab1',
            'createtable tab2 (A,B,C)',
            'listtable',
            'droptable tab2',
            'listtable',
            'dropdb',
            'listtable',
            'listdb',
            'opendb testdb',
            'listtable',
            'stop'
        ]
        responses = [
            '\r\nliste de DB\r\n-----------\r\ntestdb\r\npouet\r\ndefault\r\n',
            '\r\npas de DB choisi\r\n',
            '\r\n',
            '\r\ntestdb\r\n',
            '\r\nliste des tables\r\n----------------\r\ntab1\r\n',
            '\r\nliste de champ\r\n--------------\r\ncol3\r\ncol1\r\ncol2\r\n',
            '\r\nliste de champ\r\n--------------\r\ncol3 \| col1 \| col2\r\n3 \| 1 \| 2\r\n6 \| 4 \| 5\r\n9 \| 7 \| 8\r\n',
            '\r\n',
            '\r\nliste de champ\r\n--------------\r\ncol3 \| col1 \| col2\r\n3 \| 1 \| 2\r\n6 \| 4 \| 5\r\n9 \| 7 \| 8\r\n8 \| 9 \| 10\r\n',
            '\r\n',
            '\r\nliste des tables\r\n----------------\r\ntab1\r\ntab2\r\n',
            '\r\n',
            '\r\nliste des tables\r\n----------------\r\ntab1\r\n',
            '\r\n',
            '\r\npas de DB choisi\r\n',
            '\r\nliste de DB\r\n-----------\r\ntestdb\r\npouet\r\ndefault\r\n',
            '\r\n',
            '\r\npas de table\r\n',
            '\r\n'
        ]

        for cmd, res in zip(cmds, responses):
            cli = 'NazeDBCLI ("stop" to quit): {}'.format(cmd)
            child.sendline(cmd)
            child.expect(res)
            after, before = child.after.decode(), child.before.decode()
            assert before == cli and re.match(res, after).string == after

if __name__ == '__main__':
    unittest.main()
