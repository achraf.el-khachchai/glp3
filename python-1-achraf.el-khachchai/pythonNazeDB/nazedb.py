import os
from collections import namedtuple

DBInfos = namedtuple('infos', ['dbname', 'dbpath', 'dbfile'])

class NazeDB(object):

    _index = '.index.db'

    def __init__(self, dbname: str=None):
        if dbname is not None:
            self.open(dbname)
            if self._dbinfos is None:
                os.mkdir(dbname)
                open('{}/{}'.format(dbname, NazeDB._index), 'a').close()
                dbdir = './{}'.format(dbname)
                self._dbinfos = DBInfos._make([dbname, dbdir, NazeDB._index])

    def open(self, dbname: str):
        self._dbinfos = self._finddb(dbname)

    @property
    def dbinfos(self) -> DBInfos:
        return self._dbinfos

    @staticmethod
    def listdb() -> [DBInfos]:
        return [DBInfos._make([os.path.basename(root), root, NazeDB._index])
                for root, dirs, files in os.walk('.')
                if NazeDB._indexinfiles(files)]

    @staticmethod
    def _indexinfiles(files: [str]) -> bool:
        try:
            i = files.index(NazeDB._index)
            return True
        except ValueError:
            return False

    def _finddb(self, dbname: str) -> (DBInfos or None):
        for root, dirs, files in os.walk('.'):
            name = os.path.basename(root)
            if name == dbname and NazeDB._indexinfiles(files):
                return DBInfos._make([name, root, NazeDB._index])
        return None

    def _indexpath(self) -> str:
        return '{}/{}'.format(self.dbinfos.dbpath, NazeDB._index)

    def _tablenames(self) -> [str]:
        with open(self._indexpath()) as findex:
            return findex.read().split('\n')[:-1]
