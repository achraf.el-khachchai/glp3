#!/usr/bin/env python3

import csv
import argparse
import mechanicalsoup

parser = argparse.ArgumentParser(description='crawl cli argument parser')

parser.add_argument('-u', '--url', action='store', dest='url')
parser.add_argument('-g', '--get', action='store', dest='get')
parser.add_argument('-p', '--put', action='store', dest='put')

browser = mechanicalsoup.Browser()
divs = ['name', 'lastname', 'login', 'desc']

def build_url(url: str, sub: str):
    return url + sub[1:] if sub.startswith('/') else sub

def get(csvname: str, url: str):
    page = browser.get('{}list_user'.format(url))
    links = page.soup.find_all('a')
    with open(csvname, 'w') as csvfile:
        csvfile.write('{}\n'.format('name,lastname,login,desc'))
        for link in links:
            page = browser.get(build_url(url, link['href']))
            line = ','.join([page.soup.find("div", class_=div).contents[0]
                for div in divs])
            csvfile.write('{}\n'.format(line))

def put(csvname: str, url: str):
    page = browser.get('{}add_user'.format(url))
    form = page.soup.find_all('form')[0]
    with open(csvname) as csvfile:
        reader = list(csv.reader(csvfile, delimiter=','))
        head = reader[0]
        rows = reader[1:]
        indexes = { div: head.index(div) for div in divs }
        for row in rows:
            for div in divs:
                form.find(id=div)['value'] = row[indexes[div]]
            browser.submit(form, build_url(url, form['action']))
