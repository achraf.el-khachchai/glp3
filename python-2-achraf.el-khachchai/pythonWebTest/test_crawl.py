#!/usr/bin/env python3

import os
import csv
import unittest
import bot

class TestCrawl(unittest.TestCase):

    dbfile = '../pythonWeb/user.db'
    url = 'http://localhost:5000/'

    @classmethod
    def setUpClass(cls):
        if os.path.exists(cls.dbfile):
            os.remove(cls.dbfile)

    @classmethod
    def tearDownClass(cls):
        os.remove(cls.dbfile)
        os.remove('empty.csv')
        os.remove('get.csv')

    def test_1_get_empty(self):
        bot.get('empty.csv', self.url)
        with open('empty.csv') as csvfile:
            self.assertEqual(csvfile.read(), 'name,lastname,login,desc\n')

    def test_2_populate_empty(self):
        bot.put('user.csv', self.url)
        with open('user.csv') as userfile:
            bot.get('get.csv', self.url)
            with open('get.csv') as getfile:
                self.assertEqual(userfile.read(), getfile.read())

    def test_3_populate_populated(self):
        bot.put('user.csv', self.url)
        with open('user.csv') as userfile:
            urows = list(csv.reader(userfile, delimiter=','))[1:]
            urows = [*urows, *urows]
            bot.get('get.csv', self.url)
            with open('get.csv') as getfile:
                getrows = list(csv.reader(getfile, delimiter=','))[1:]
                self.assertEqual(urows, getrows)

if __name__ == '__main__':
    unittest.main()
