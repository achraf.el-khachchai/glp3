#!/usr/bin/env python3

import os
import csv
import unittest
from model import Query, User

class TestModel(unittest.TestCase):

    dbfile = 'test.db'

    @classmethod
    def tearDownClass(cls):
        os.remove(cls.dbfile)

    def test_1_empty_list(self):
        assert not Query(self.dbfile).all_users()

    def test_2_populate_empty_list(self):
        Query(self.dbfile).add_user('a', 'A', 'A_a', 'aA')
        Query(self.dbfile).add_user('b', 'B', 'B_b', 'bB')
        res = [
                {'uid': 0, 'login': 'A_a'},
                {'uid': 1, 'login': 'B_b'}
        ]
        assert Query(self.dbfile).all_users() == res

    def test_3_add_populate_populated_lis(self):
        Query(self.dbfile).add_user('a', 'C', 'C_a', 'aC')
        Query(self.dbfile).add_user('b', 'D', 'D_b', 'bD')
        res = [
                {'uid': 0, 'login': 'A_a'},
                {'uid': 1, 'login': 'B_b'},
                {'uid': 2, 'login': 'C_a'},
                {'uid': 3, 'login': 'D_b'}
        ]
        assert Query(self.dbfile).all_users() == res

    def test_4_select_user_by_id(self):
        res = User(uid=2, name='a', lastname='C', login='C_a', desc='aC')
        user = Query(self.dbfile).user_by_id(2)
        assert repr(res) == repr(user)

if __name__ == '__main__':
    unittest.main()
