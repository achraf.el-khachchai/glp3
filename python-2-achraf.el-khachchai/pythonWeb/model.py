from sqlalchemy import Column, Integer, String, create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import Session

Base = declarative_base()

class User(Base):
    __tablename__ = 'User'

    uid = Column(Integer, primary_key=True)
    name = Column(String(20))
    lastname = Column(String(20))
    login = Column(String(10))
    desc = Column(String(100))

    def __repr__(self):
        return """User(uid={uid}, login={login!r}, name={name!r},
    lastname={lastname!r}, desc={desc!r})""".format(**vars(self))

class Query(object):

    def __init__(self, dbfile: str):
        self.engine = create_engine('sqlite:///{}'.format(dbfile))
        Base.metadata.create_all(self.engine)
        self.session = Session(self.engine)

    def add_user(self, name, lastname, login, desc):
        Base.metadata.create_all(self.engine)
        user = User(uid=self.session.query(User).count(), name=name,
                lastname=lastname, login=login, desc=desc)
        self.session.add(user)
        self.session.commit()

    def all_users(self):
        users = self.session.query(User).all()
        return [{'uid': user.uid, 'login': user.login} for user in users]

    def user_by_id(self, uid):
        query = self.session.query(User).filter_by(uid=uid)
        return query.all()[0]
