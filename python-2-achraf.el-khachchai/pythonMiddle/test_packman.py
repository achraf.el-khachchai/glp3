#!/usr/bin/env python3

import packman
import unittest

class TestPackman(unittest.TestCase):

    def test_ushort_uint(self):
        buf = b'\x01\x42\x00\x01\x02\x03'
        res = (322, 66051)
        assert packman.ushort_uint(buf) == res
        assert packman.ushort_uint(bytearray(buf)) == res

    def test_buf2latin(self):
        buf = b'\x00\x04G\xE9g\xe9zzz'
        res = (4, 'Gégé')
        assert packman.buf2latin(buf) == res
        assert packman.buf2latin(bytearray(buf)) == res

    def test_ascii2buf(self):
        res = b'\x00\x00\x00\x04\x00\x01I\x00\x04like\x00\x03the\x00\x04game'
        assert packman.ascii2buf("I", "like", "the", "game") == bytearray(res)

if __name__ == '__main__':
    unittest.main()
