import struct

# TODO: both long 'L' & int 'I' are 8 bytes long, which one use
def ushort_uint(buf: (bytes or bytearray)) -> (int, int):
    return struct.unpack('>HL', buf)

def buf2latin(buf: (bytes or bytearray)) -> (int, str):
    size = struct.unpack('>H', buf[:2])[0]
    return (size, buf[2:2+size].decode('Latin-1'))

def ascii2buf(*args):
    buf = bytearray(struct.pack('>L', len(args)))
    sizes = list(map(lambda x: struct.pack('>H', len(x)), args))
    strings = list(map(lambda x: bytes(x, 'ascii'), args))
    for size, string in zip(sizes, strings):
        buf = buf + size + string
    return buf
